<?php

if (isset($_POST)) {
    foreach ($_POST as $key => $val) {
        if ($val !== '0' && empty($val)) {
            header('Location: index.php');
        }
    }

    if (!empty($_POST['id']) && !empty($_POST['first-name']) && !empty($_POST['last-name'] )) {
        $student = Student::newStudent($_POST['id'], $_POST['first-name'], $_POST['last-name']);

        foreach ($courseListArr as $shortHand => $course) {
            $student->setMarks($course, (int)$_POST[$shortHand]);
        }

        storeInFile($student->getStudentName(), $student->getStudentId(), $student->getMarks());

        header('Location: view/view_file.php');
    }
}

function storeInFile($studentName, $studentId, $studentMarksArr) {
    $store = <<<CONTENT
*******************************************
| NAME: $studentName
| StudentID: $studentId
*******************************************
|   SUBJECT   |    MARKS    |    GRADE    |
*******************************************
CONTENT;

    $bottomFrame = <<<CONTENT
*******************************************

CONTENT;

    $file = file_put_contents(VIEW . DS . 'result.txt', $store, FILE_APPEND);

    foreach ($studentMarksArr as $courseName => $courseMarks) {
        $grade = gradePoint($courseMarks);
        $str = calculateSpace($courseName, $courseMarks, $grade);

        $file .= file_put_contents(VIEW . DS . 'result.txt', PHP_EOL, FILE_APPEND);
        $file .= file_put_contents(VIEW . DS . 'result.txt', $str, FILE_APPEND);
    }

    $file .= file_put_contents(VIEW . DS . 'result.txt', $bottomFrame, FILE_APPEND);
}

function calculateSpace($sub, $marks, $grade, $fieldLength = 13) {
    $str = '';
    $subGap = $fieldLength - strlen($sub);
    $marksGap = $fieldLength - strlen($marks);
    $gradeGap = $fieldLength - strlen($grade);

    $str .= '| ' . $sub . str_repeat(' ', $subGap - 1) . '|';
    $str .= str_repeat(' ', $marksGap - 1) . $marks . ' |';
    $str .= str_repeat(' ', $gradeGap - 1) . $grade . ' |';
    $str .= PHP_EOL;

    return $str;
}
