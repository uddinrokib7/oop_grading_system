<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Form</title>
</head>
<body>

<form method="post" action="">
    <h3>Student Information</h3>
    <label for="first-name">First Name : </label>
    <input type="text" name="first-name" id="first-name" value="<?= isset($_POST['first-name']) ? $_POST['first-name'] : ''; ?>">
    <br>
    <label for="last-name">Last Name : </label>
    <input type="text" name="last-name" id="last-name" value="<?= isset($_POST['last-name']) ? $_POST['last-name'] : ''; ?>">
    <br>
    <label for="id">Student ID : </label>
    <input type="text" name="id" id="id" value="<?= isset($_POST['id']) ? $_POST['id'] : ''; ?>">
    <br>

    <h3>Course Marks</h3>
    <?php foreach ($courseListArr as $shortHand => $course): ?>
    <label for="<?= $shortHand; ?>"><?= $course; ?> : </label>
    <input type="number" name="<?= $shortHand; ?>" id="<?= $shortHand; ?>" min="0" max="100" value="<?= isset($_POST[$shortHand]) ? $_POST[$shortHand] : ''; ?>">
    <br>
    <?php endforeach; ?>

    <input type="submit">
</form>
<br>
<a href="view/view_file.php">View Stored File</a>
</body>
</html>