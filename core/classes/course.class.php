<?php

class Course {
    protected $courseName;
    protected $marks;

    /*
    ----------------------------------------------------------------------------------------
    | A private access modifier prevents the Class from directly constructing from outside |
    ----------------------------------------------------------------------------------------
    */
    private function __construct($courseName, $marks) {
        $this->setCourseName($courseName);
        $this->setCourseMarks($marks);
    }

    public function getCourseName() {
        return isset($this->courseName) ? $this->courseName : false;
    }

    public function getCourseMarks() {
        return isset($this->marks) ? $this->marks : false;
    }

    protected function setCourseName($name) {
        $this->courseName = $name;
    }

    protected function setCourseMarks($num) {
        $this->marks = $num;
    }

    /*
    -------------------------------------------------------------
    | Creates a new object of Course class                      |
    | @parameter Array $courseArr['course-name'=> , 'marks'=> ] |
    | @return Object of Course Class                            |
    -------------------------------------------------------------
    */
    static function newCourse($courseName, $marks) {
        return new static($courseName, $marks);
    }
}