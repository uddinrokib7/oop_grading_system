<?php

class Student {
    protected $firstName;
    protected $lastName;
    protected $fullName;
    protected $id;
    protected $courses = [];

    /*
    ----------------------------------------------------------------------------------------
    | A private access modifier prevents the Class from directly constructing from outside |
    ----------------------------------------------------------------------------------------
    */
    private function __construct($id, $fn, $ln) {
        $this->setStudentId($id);
        $this->setStudentName($fn, $ln);
    }

    public function getStudentName($portion = 'full') {
        switch ($portion) {
            case 'full' :
                return isset($this->fullName) ? $this->fullName : false;
            case 'first' :
                return isset($this->firstName) ? $this->firstName : false;
            case 'last' :
                return isset($this->lastName) ? $this->lastName : false;
            default:
                return trigger_error('The parameter ' . $portion . ' is invalid in ' . __METHOD__, E_USER_ERROR);
        }

    }

    public function getStudentId() {
        return isset($this->id) ? $this->id : false;
    }

    protected function getCourseMarks() {
        $marks = [];
        foreach ($this->courses as $course) {
            $marks[$course->getCourseName()] = $course->getCourseMarks();
        }
        return $marks;
    }

    public function getMarks() {
        if (count($this->courses)) {
            $marks = $this->getCourseMarks();
            return $marks;
        }

        return false;
    }

    protected function setStudentName($fn, $ln = '') {
        $this->firstName = $fn;
        $this->lastName = $ln;
        $fullName = $this->firstName . ' ' . $this->lastName;
        $this->fullName = $fullName;
    }

    protected function setStudentId($id) {
        $this->id = $id;
    }

    public function setMarks($courseName, $marks) {
        $this->courses[] = Course::newCourse($courseName, $marks);
    }

    /*
    -----------------------------------------------------------------------
    | Creates a new object of Student class                               |
    | @parameter Array $infoArr['id'=> , 'first-name'=> , 'last-name'=> ] |
    | @return Object of Student Class                                     |
    -----------------------------------------------------------------------
    */
    static function newStudent($id, $fn, $ln) {
        return new static($id, $fn, $ln);
    }
}


