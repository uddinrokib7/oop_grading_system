<?php

function gradePoint($num) {
    switch (true) {
        case ($num >= 80):
            return 'A+';
        case ($num >= 70):
            return 'A';
        case ($num >= 65):
            return 'A-';
        case ($num >= 60):
            return 'B+';
        case ($num >= 55):
            return 'B';
        case ($num >= 50):
            return 'B-';
        case ($num >= 45):
            return 'C';
        case ($num >= 40):
            return 'C-';
        case ($num >= 33):
            return 'D';
        case ($num < 33):
            return 'F';
        default:
            trigger_error('The parameter ' . $num . ' in ' . __FUNCTION__ . ' is invalid', E_USER_ERROR);
    }
}